/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */
package com.mycompany.lab2;

import java.util.Random;
import java.util.Scanner;

/**
 *
 * @author fkais
 */
public class Lab2 {

    static boolean playAgain = true;
    static char[][] table;
    static char[][] boardChar;
    static char turn;

    public Lab2() {
        boardChar = new char[3][3];
        turn = 'X';
        table = new char[3][3];
        Board();
    }

    private static void printStartGame() {
        System.out.println("Welcome to Play Game OX");
    }

    private static void printThank() {
        System.out.println("Thank you for playing");
    }

    private static void Board() {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                boardChar[i][j] = '-';
                table[i][j] = '-';
            }
        }
    }

    private static void printTable() {
        System.out.println("-------------");
        for (int i = 0; i < 3; i++) {
            System.out.print("| ");
            for (int j = 0; j < 3; j++) {
                System.out.print(table[i][j] + " | ");
            }
            System.out.println("");
            System.out.println("-------------");
        }
    }

    private static void inputMove() {
        if (turn == 'X') {
            Scanner sc = new Scanner(System.in);
            System.out.print("Please enter your move (1-9): ");
            int move = sc.nextInt();
            if (checkMove(move)) {
                int Row = (move - 1) / 3;
                int Col = (move - 1) % 3;
                table[Row][Col] = turn;
                switchMove();
            }
        } else {
            Random bot = new Random();
            int move;
            do {
                move = bot.nextInt(8) + 1;
            } while (!checkMove(move));
            int Row = (move - 1) / 3;
            int Col = (move - 1) % 3;
            table[Row][Col] = turn;
            switchMove();
        }
    }

    private static boolean gameDraw() {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if (table[i][j] == '-') {
                    return false;
                }
            }
        }
        return true;
    }

    private static void printDraw() {
        System.out.println("It's a draw!");
    }

    private static void switchMove() {
        if (turn == 'X') {
            turn = '0';
        } else {
            turn = 'X';
        }
    }

    private static boolean checkWinner() {
        return checkRow() || checkCol() || checkDiagnol();
    }

    private static boolean checkDiagnol() {
        for (int i = 0; i < 3; i++) {
            if (table[0][0] != '-' && table[0][0] == table[1][1] && table[0][0] == table[2][2]) {
                return true;
            }
        }

        for (int i = 0; i < 3; i++) {
            if (table[0][2] != '-' && table[0][2] == table[1][1] && table[0][2] == table[2][0]) {
                return true;
            }
        }
        return false;
    }

    private static boolean checkRow() {
        for (int i = 0; i < 3; i++) {
            if (table[i][0] != '-' && table[i][0] == table[i][1] && table[i][0] == table[i][2]) {
                return true;
            }
        }
        return false;
    }

    private static boolean checkCol() {
        for (int i = 0; i < 3; i++) {
            if (table[0][i] != '-' && table[0][i] == table[1][i] && table[0][i] == table[2][i]) {
                return true;
            }
        }
        return false;
    }

    private static boolean checkMove(int move) {
        int Row = (move - 1) / 3;
        int Col = (move - 1) % 3;
        if (move < 1 || move > 9) {
            if (turn != 'X') {
                return false;
            }
            System.out.println("Invalid move, please input number 1-9");
            return false;
        }
        if (table[Row][Col] != '-') {
            if (turn != 'X') {
                return false;
            }
            System.out.println("Invalid move, Position already taken");
            return false;
        }
        return true;
    }

    private static void printTurn() {
        System.out.println("Turn: " + turn);
    }

    private static void Winner() {
        switchMove();
        System.out.println("Game Over!! " + turn + "'s is winner!!");
    }

    private static void resetGame() {
        Scanner sc = new Scanner(System.in);
        System.out.print("Do you want to play again? (Y/N): ");
        String reset = sc.next().trim().toLowerCase();
        playAgain = reset.equals("y");
    }

    public void playGame() {
        printStartGame();
        while (playAgain) {
            Board();
            while (true) {
                printTable();
                printTurn();
                inputMove();
                if (checkWinner()) {
                    printTable();
                    Winner();
                    break;
                }
                if (gameDraw()) {
                    printTable();
                    printDraw();
                    break;
                }
            }
            resetGame();
        }
        printThank();
    }

    public static void main(String[] args) {
        Lab2 game = new Lab2();
        game.playGame();
    }
}
